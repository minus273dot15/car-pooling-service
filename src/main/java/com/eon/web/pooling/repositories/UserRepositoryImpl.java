package com.eon.web.pooling.repositories;

import com.eon.web.pooling.models.User;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.LocalDateTime;
import java.util.List;
@Repository
public class UserRepositoryImpl implements UserRepository{
    private final SessionFactory sessionFactory;
    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAll() {
        try(Session session = sessionFactory.openSession()){
            Query<User> query = session.createQuery("from User",User.class);
            return query.list();
        }
    }

    @Override
    public User getUserById(int id) throws EntityNotFoundException {
        try(Session session = sessionFactory.openSession()){
            String hql = "Select u from User u Where u.id = :user_id";
            Query<User> query = session.createQuery(hql,User.class);
            query.setParameter("user_id",id);
            User user = query.uniqueResult();
            if (user == null) {
                throw new EntityNotFoundException("User with id " + id + " not found");
            }
            return query.uniqueResult();
        }
    }
    @Override
    public User getUserByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            String hql = "SELECT u FROM User u WHERE u.username = :username";
            Query<User> query = session.createQuery(hql, User.class);
            query.setParameter("username", username);
            return query.uniqueResult();
        }
    }

    @Override
    public User getUserByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            String hql = "SELECT u FROM User u WHERE u.email = :email";
            Query<User> query = session.createQuery(hql, User.class);
            query.setParameter("email", email);
            return query.uniqueResult();
        }
    }

    @Override
    public void create(User user) {
        try(Session session = sessionFactory.openSession()){
            user.setCreateDate(LocalDateTime.now());
            user.setVerified(0);
            user.setUserLevel(0);
            user.setPhotoVerification(false);
            user.setTFA(false);
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        }
    }


}
