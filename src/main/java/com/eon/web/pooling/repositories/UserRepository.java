package com.eon.web.pooling.repositories;

import com.eon.web.pooling.models.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository {

    List<User> getAll();

    User getUserById(int id);

    User getUserByUsername(String username);

    void create(User user);

    User getUserByEmail(String email);
}
