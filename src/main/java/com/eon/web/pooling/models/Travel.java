package com.eon.web.pooling.models;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name="Travels")
public class Travel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "travel_id")
    private int travelId;
    @ManyToOne
    @JoinColumn(name="driver_id")
    private UserDriverEntity userDriver;
    @OneToOne
    @JoinColumn(name="starting_point")
    private City startingPoint;

    @OneToOne
    @JoinColumn(name="ending_point")
    private City endingPoint;
    @Column(name="free_spots")
    private int freeSpots;
    @Column(name="comment")
    private String comment;
    @Column(name= "trip_status")
    private String tripStatus;
    @Column(name = "cost")
    private BigDecimal cost;

    @ManyToOne
    @JoinColumn(name="car_id")
    private Car car;
    @Column(name="departure_time")
    private LocalDateTime departureTime;

    public Travel() {
    }

    public Travel(int travelId, UserDriverEntity userDriver, City startingPoint,
                  City endingPoint, int freeSpots, String comment, String tripStatus,
                  BigDecimal cost, Car car, LocalDateTime departureTime) {
        this.travelId = travelId;
        this.userDriver = userDriver;
        this.startingPoint = startingPoint;
        this.endingPoint = endingPoint;
        this.freeSpots = freeSpots;
        this.comment = comment;
        this.tripStatus = tripStatus;
        this.cost = cost;
        this.car = car;
        this.departureTime = departureTime;
    }

    public int getTravelId() {
        return travelId;
    }

    public void setTravelId(int travelId) {
        this.travelId = travelId;
    }

    public UserDriverEntity getUserDriver() {
        return userDriver;
    }

    public void setUserDriver(UserDriverEntity userDriver) {
        this.userDriver = userDriver;
    }

    public City getStartingPoint() {
        return startingPoint;
    }

    public void setStartingPoint(City startingPoint) {
        this.startingPoint = startingPoint;
    }

    public City getEndingPoint() {
        return endingPoint;
    }

    public void setEndingPoint(City endingPoint) {
        this.endingPoint = endingPoint;
    }

    public int getFreeSpots() {
        return freeSpots;
    }

    public void setFreeSpots(int freeSpots) {
        this.freeSpots = freeSpots;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getTripStatus() {
        return tripStatus;
    }

    public void setTripStatus(String tripStatus) {
        this.tripStatus = tripStatus;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public LocalDateTime getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(LocalDateTime departureTime) {
        this.departureTime = departureTime;
    }
}
