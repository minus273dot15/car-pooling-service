package com.eon.web.pooling.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

import java.time.LocalDateTime;
import java.util.Objects;
@Getter
@Setter
@Entity
@Table(name = "Users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int userId;
    @Column(name = "username")
    private String username;

    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "email")
    private String email;
    @Column (name ="password")
    private String password;
    @Column (name= "user_level")
    private int userLevel;

    @Column (name= "verified")
    private int verified;
    @Column(name="verification_code")
    private String verificationCode;
    @Column (name = "enabled")
    private boolean enabled;
    @Column (name="profile_picture")
    private byte [] profilePicture;
    @Column (name="phone")
    private String  phoneNumber;
    @Column(name="status_deleted")
    private boolean statusDeleted;
    @Column(name ="create_date")
    private LocalDateTime createDate;
    @Column (name="last_update")
    private LocalDateTime lastUpdate;
    @Column(name="photo_verification")
    private boolean photoVerification;
    @Column (name="TFA")
    private boolean TFA;
    public User() {

    }

    public User(int userId, String username, String firstName, String lastName,
                String email, String password, int userLevel, int verified,
                String verificationCode, boolean enabled, byte[] profilePicture,
                String phoneNumber, boolean statusDeleted, LocalDateTime createDate,
                LocalDateTime lastUpdate, boolean photoVerification, boolean TFA) {
        this.userId = userId;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.userLevel = userLevel;
        this.verified = verified;
        this.verificationCode = verificationCode;
        this.enabled = enabled;
        this.profilePicture = profilePicture;
        this.phoneNumber = phoneNumber;
        this.statusDeleted = statusDeleted;
        this.createDate = createDate;
        this.lastUpdate = lastUpdate;
        this.photoVerification = photoVerification;
        this.TFA = TFA;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return userId == user.userId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId);
    }
}
