package com.eon.web.pooling.models;

import javax.persistence.*;

@Entity
@Table(name="Car")
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="car_id")
    private int carId;
}
