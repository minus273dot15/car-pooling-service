package com.eon.web.pooling.helpers;

public class AuthorizationException extends RuntimeException {
    public AuthorizationException (String message) {
        super(message);
    }
}
