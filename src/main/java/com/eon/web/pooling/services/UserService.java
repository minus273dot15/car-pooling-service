package com.eon.web.pooling.services;

import com.eon.web.pooling.models.User;

import java.util.List;

public interface UserService {

    List<User> getAll();

    User getUserById(int id);

    User getUserByUsername(String username);

    User getUserByEmail(String email);

    void create(User user);
}
