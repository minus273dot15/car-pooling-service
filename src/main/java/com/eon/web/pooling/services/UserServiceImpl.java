package com.eon.web.pooling.services;

import com.eon.web.pooling.exceptions.EntityDuplicateException;
import com.eon.web.pooling.models.User;
import com.eon.web.pooling.repositories.UserRepository;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public User getUserById(int id) {
        return userRepository.getUserById(id);
    }

    @Override
    public User getUserByUsername(String username) {
        User user = userRepository.getUserByUsername(username);
        if(user==null){
            throw new EntityNotFoundException("User with username "+ username+" not found");
        }
        return user;
    }
    @Override
    public User getUserByEmail(String email) {
        return userRepository.getUserByEmail(email);
    }

    @Override
    public void create(User user) {
        User existingUser = userRepository.getUserByEmail(user.getEmail());
        if (existingUser !=null){
            throw new EntityDuplicateException("User","email",user.getEmail());
        }
        existingUser=userRepository.getUserByUsername(user.getUsername());
        if(existingUser!=null){
            throw  new EntityDuplicateException("User","username", user.getUsername());
        }

        user.setEmail(user.getEmail());;
        user.setVerificationCode(RandomString.make(64));
        user.setEnabled(false);
        user.setFirstName(user.getFirstName());
        user.setLastName(user.getLastName());
        user.setPassword(user.getPassword());
        userRepository.create(user);
    }
}
