package com.eon.web.pooling.controllers.RestController;

import com.eon.web.pooling.exceptions.EntityDuplicateException;
import com.eon.web.pooling.helpers.AuthenticationHelper;
import com.eon.web.pooling.helpers.AuthorizationException;
import com.eon.web.pooling.helpers.UserMapper;
import com.eon.web.pooling.models.DTO.UserDto;
import com.eon.web.pooling.models.User;
import com.eon.web.pooling.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserRestController {
    private final UserService userService;
   private final UserMapper userMapper;
   private final AuthenticationHelper authenticationHelper;
    @Autowired
    public UserRestController(UserService userService, UserMapper userMapper, AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<UserDto> getAll(){
        try {
            List<User> users =userService.getAll();
            return userMapper.toDtoList(users);
        }
        catch (AuthorizationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
    @GetMapping("/{id}")
    public UserDto getUserById (@PathVariable int id){
        try {
            User user = userService.getUserById(id);
            return userMapper.toDto(user);
        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    @GetMapping("/username/{username}")
    public UserDto getUserByUsername(@PathVariable String username){
        try {
            User user = userService.getUserByUsername(username);
            return userMapper.toDto(user);
            } catch (EntityNotFoundException e ){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    @PostMapping("/register")
    public UserDto createUser(@Valid @RequestBody UserDto userDto){
        try{

        User user = userMapper.toEntity(userDto);
        userService.create(user);
        return userMapper.toDto(user);
    } catch (EntityDuplicateException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT,e.getMessage());
        }
    }
}
